# data_parser

## Install rust
```
curl https://sh.rustup.rs -sSf | sh
```

## Clone the repo
```
git clone git@bitbucket.org:vihu89/data_parser.git
```

## Build dev release
```
cd data_parser
cargo build
```

## Build prod release
```
cd data_parser
cargo build --release
```

## Run dev release
```
cd data_parser
./target/debug/data_parser
```

## Run prod release
```
cd data_parser
./target/release/data_parser
```

## Help
```
cd data_parser
./target/debug/data_parser -h
```
