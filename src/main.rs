extern crate csv;
#[macro_use]
extern crate serde_derive;
extern crate clap;
extern crate url;

use std::error::Error;
use std::process;
use clap::{Arg, App};
use csv::{Reader, Writer};
use url::Url;

#[derive(Debug, Deserialize)]
struct InRecord {
    url: String
}

#[derive(Debug, Serialize)]
struct Row<'a> {
    scheme: String,
    host_str: Option<&'a str>,
    domain: Option<&'a str>,
    port: Option<u16>,
    query: Option<&'a str>,
    path: String,
    fragment: Option<&'a str>
}

// TODO: refactor into modules
fn read_csvfile(infile: &str, outfile: &str) -> Result<(), Box<Error>> {
    let mut rdr = Reader::from_path(infile)?;
    let mut wtr = Writer::from_path(outfile)?;
    for result in rdr.deserialize() {
        let record: InRecord = result?;
        let parsed = Url::parse(&record.url)?;
        wtr.serialize(Row {
            scheme: parsed.scheme().to_string(),
            host_str: parsed.host_str(),
            domain: parsed.domain(),
            port: parsed.port(),
            query: parsed.query(),
            path: parsed.path().to_string(),
            fragment: parsed.fragment(),
        })?;
    }
    wtr.flush()?;
    Ok(())
}

fn main() {
    let matches = App::new("data_parser")
        .version("1.0")
        .author("Rahul Garg")
        .about("Does csv parsing of ONLY single column csv with url header")
        .arg(Arg::with_name("infile")
             .short("i")
             .long("infile")
             .value_name("INFILE")
             .help("Sets the csv file to parse")
             .takes_value(true))
        .arg(Arg::with_name("outfile")
             .short("o")
             .long("outfile")
             .value_name("OUTFILE")
             .help("Sets the output csv file after parsing")
             .takes_value(true))
        .get_matches();
    let infile = matches.value_of("infile").unwrap_or("./data.csv");
    let outfile = matches.value_of("outfile").unwrap_or("./out.csv");
    if let Err(err) = read_csvfile(infile, outfile) {
        println!("error running example: {}", err);
        process::exit(1);
    }
}
